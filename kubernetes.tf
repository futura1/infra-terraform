provider "kubernetes" {
  config_path    = "~/projects2/kubeconfig"
  config_context = "default-context"
}

resource "kubernetes_namespace" "ns" {
  for_each = toset(["production", "development", "vault", "ingress-nginx"])
  metadata {
    name = each.value
  }
}
