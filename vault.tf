provider "vault" {}

provider "random" {}

resource "vault_mount" "kv" {
  type = "kv-v2"
  path = "kv"
}

resource "random_password" "production" {
  length = 32
}

resource "vault_generic_secret" "production" {
  path = "kv/production/simple-app"

  data_json = jsonencode({
    password = random_password.production.result
  })
}

resource "random_password" "development" {
  length = 32
}

resource "vault_generic_secret" "development" {
  path = "kv/development/simple-app"

  data_json = jsonencode({
    password = random_password.development.result
  })
}

resource "vault_auth_backend" "kubernetes" {
  type = "kubernetes"
}

data "vault_generic_secret" "simpleapp_database_vault" {
  path = "kv/infra/kubernetes-auth"
}

resource "vault_kubernetes_auth_backend_config" "kubernetes" {
  depends_on = [
    vault_mount.kv
  ]

  backend                = vault_auth_backend.kubernetes.path
  kubernetes_host        = "https://kubernetes.default"
  disable_iss_validation = true
  kubernetes_ca_cert     = data.vault_generic_secret.simpleapp_database_vault.data["ca.crt"]
  token_reviewer_jwt     = data.vault_generic_secret.simpleapp_database_vault.data["token"]
}

resource "vault_policy" "production_simpleapp" {
  name   = "production-simpleapp"
  policy = <<-EOF
    path "kv/data/production/simple-app" {
      capabilities = ["read"]
    }
  EOF
}

resource "vault_kubernetes_auth_backend_role" "production" {
  backend   = vault_auth_backend.kubernetes.path
  role_name = "production-simpleapp"

  bound_service_account_names      = ["simple-app"]
  bound_service_account_namespaces = ["production"]

  token_policies = [vault_policy.production_simpleapp.name]
}

resource "vault_policy" "development_simpleapp" {
  name   = "development-simpleapp"
  policy = <<-EOF
    path "kv/data/development/simple-app" {
      capabilities = ["read"]
    }
  EOF
}

resource "vault_kubernetes_auth_backend_role" "development" {
  backend   = vault_auth_backend.kubernetes.path
  role_name = "development-simpleapp"

  bound_service_account_names      = ["simple-app"]
  bound_service_account_namespaces = ["development"]

  token_policies = [vault_policy.development_simpleapp.name]
}



# data "vault_generic_secret" "simpleapp_database_vault" {
#   path = "kv/production/simpleapp-database-vault"
# }

# resource "vault_mount" "database" {
#   type = "database"
#   path = "database"
# }

# locals {
#   prod_vault_db_password = data.vault_generic_secret.simpleapp_database_vault.data["password"]
# }

# resource "vault_database_secret_backend_connection" "simpleapp_production_backend" {
#   name    = "simpleapp-production-backend"
#   backend = vault_mount.database.path
#   postgresql {
#     connection_url = "postgresql://vault-postgres:" + local.prod_vault_db_password + "@simple-app-postgres.production.cluster.local:5432/simpleapp?sslmode=disable"
#   }
# }

# resource "vault_database_secret_backend_role" "simpleapp_production_backend" {
#   backend = vault_database_secret_backend_connection.simpleapp_production_backend.backend
#   db_name = vault_database_secret_backend_connection.simpleapp_production_backend.name
#   name    = "simpleapp-production-backend-app"

#   creation_statements = [
#     "CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}' IN ROLE 'postgres';"
#   ]
# }
