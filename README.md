# infra-terraform

## Issues

- No CI/CD workflow
- Kubernetes cluster setup not included
- Vault
    - No auth method for terraform, currently using applier env

## Manual secrets

```
- `kv/infra/kubernetes-auth`
```

```json
{
    "ca.crt": "",
    "token": ""
}
```